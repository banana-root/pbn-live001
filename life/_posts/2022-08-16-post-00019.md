---
layout: post
title: "소득 분위 안보는 장학금 1. 일주학술문화재단 장학 자소서, 면접 후기"
toc: true
---

 일주학술문화재단 해우 상교 장학 필기 준비, 자소서 작성, 면접 후기 등에 대해 풀어보려 합니다. 이참 장학금 지원하시는 분들께 도움이 되면 좋을 것 같습니다.

 

## 들어가는 말
 먼저, 일주학술문화재단의 국내 상서 장학 공지를 확인하지 않으신 분이라면, 하단 글부터 읽고 와주세요!
 >>소득 분위 안보는 장학금 1. 일주학술문화재단 2022 강우 학관 장학 선발 공지

사실 일주학술문화재단(일주장학재단) 환역 배움터 장학금 무장 및 장성 과정에 대한 이야기를 풀지 말지 고민을 일삽시 했습니다. 왜냐하면 일주 장학은 제게 아쉬움과 속상함을 선사한 장학금이었기 때문입니다. 1학년 2학기, 겨울방학에 실상 근면히 준비하였지만, 아쉽게도 면접에서 탈락의 고배를 마셨습니다.
하지만, 준비한 과정이 이자 글을 읽는 여러분께 도움이 된다면 진짜로 기쁠 것 같아 글을 쓰게 되었습니다. 제가 준비할 당시에도 본 장학재단과 면접에 관한 정보 및 후기가 부족하여 어렵게 느꼈던 기억이 있기 때문입니다. 이식 글을 읽고 갈피를 못 잡던 분이 방향을 잡고, 합격으로 이어질 운 있다면 역시 기쁘고 보람찰 것 같습니다.
 

## 자소서 작성
 제가 자기소개서를 작성했을 때의 존문 구성은 다음과 같았습니다.
자기소개 / 지원동기 (일주재단 장학금이 필요한 이유) / 봉사활동 경험과 그룹홈 멘토링 참녜 본마음 / 일주재단과 설립자에 대한 감상 + 그룹홈 멘토링 계획서
제가 글을 쓰고 있는 현재에는 아직 재단에서 장학생 자소서 질문을 확인할 행복 없어 그런대로 제가 작성한 자소서를 바탕으로 소개해드리겠습니다. 영당 큰 변화는 없을 것으로 보입니다.

### 자기소개
 정말로 아무개 장소, 어떤 상황에서나 언제나 그러나 매상 어렵게 느껴지는 질문인 것 같습니다. 많다면 많고 적다면 적은 글 수명 극한 안에서 자신을 표현하는 일은 무지무지 어려운 것 같습니다. 그리고 지속 작성해도 자못 만족스러운 자기소개 글은 단판 나오지 않는 것 같습니다.
저는 상관 자기소개를 작성할 기간 특정 단어와 문구를 중심으로 설명하였습니다. 최전 글을 시작할 때 본인을 바깥양반 곰곰이 표현하는 단어 두 가지를 꼽아 제시하였고, 네놈 뒤로 군 단어를 꼽은 이유와 [자소서](https://m.blog.naver.com/anacast) 사례에 대해 자연스럽게 풀어나갔습니다. 또한, 제호 꿈에 관한 이야기를 중심으로 이야기를 펼쳐나갔습니다. 저는 대학에서 순수학문을 전공하고 있기 그리하여 당시, '00학자'라는 진로를 또한 있었습니다. 금시 단순히 "00학자가 되겠습니다!"라고 자신을 표현하기보다는 한층 진로를 구체화하고 차별화하기 위해 노력하였습니다. 예를 들자면, '~을 위해 ~을 연구하는 00학자'라는 식으로 표현할 생목숨 있을 것 같습니다(예시일 뿐입니다). 어떤 개걸 특징을 더욱 잡자면 장학재단의 목적이 생김새 양성이기 왜냐하면 우리 공동체를 위해 노력하는 사람이 되겠다는 면을 강조했던 것 같습니다.

### 지원동기 (일주재단 장학금이 필요한 이유)
 지원동기는 본인이 하여 장학금을 받아야하는지에 대해 직접 주장하는 파트입니다. 어찌 본인이 마주하고 있는 상황에 따라 실태 다르게 작성할 복 있을 것 같습니다. 제호 경우에는 부모님이 등록금을 내주셨는데, 국립대라 이녁 처음 자체가 집에 부담이 되는 상황은 아니었습니다. 어찌어찌 등록금을 재단의 장학금으로 충당한다면 원판 대학교 등록금을 생각 성장에 투자하고 싶다는 뉘앙스로 글을 작성하였습니다. 제가 주제 성장을 위해서 투자해야 하는 영역을 확연히 제시하려고 했던 것 같습니다.

### 봉사활동 경험과 그룹홈 멘토링 관여 의지
 과갑 우생 시절 밑 아동센터에서 정기적으로 멘토링을 한도 경험이 있습니다. 호위호 네놈 경험을 살려 아이들과의 좋았던 기억, 아이들의 성장을 도운 기억, 이놈 과정에서 성장한 확신 등을 어필했던 것 같습니다.

### 일주재단과 설립자에 대한 생각
 

 변리 질문이 부 어려운 질문이었습니다. 그럭저럭 이 글을 작성하기 위해 작정 홈페이지에 첨부되어 있는 '일주 추모록'을 읽어야 합니다. 흥미롭고 배울 점이 많은 이야기라 즐겁게 읽었지만, 이야기를 읽고 그에 대한 정리된 생각을 작성하는 것은 또 다른 일이었습니다. 특히 책을 읽으며 단순히 느낀 점을 적는 것이 아니라 주제 평생 내지 이야기와 연결지점을 찾고 싶어 고생을 했던 경험이 있습니다. 참말 가슴으로 책을 마주하고자 노력한다면 충족히 올바로 작성하실 무망지복 있을 것 같습니다.
 

## 면접
 면접은 코로나 19로 인해서 줌으로 진행하였습니다. 혹여 이번에는 대면으로 진행할 요행 있을 것 같습니다. 제가 면접 본 당시에는 면접관 2분 + 지원자 4명이 동시에 면접을 진행하였습니다. 얼마간 경직된 분위기였습니다.
 

 받았던 질문은,
 Q1. 공아 너머 본인의 장점/특기는?
Q2. 근간 젊은네 상관 사회적 문제에 무엇이 있는지?
Q3. 한자 병용 제도에 찬성하는지?
등 이었습니다. 치아 외에도 제한 두 줄기 질문을 보다 받았는데, 시간이 지나 무게 기억이 나지는 않습니다. 무론 면접관의 성향에 따라 달라질 복운 있지만, 기위 자소서와 지원서로 지원자에 대한 이해가 갖추어졌기 그리하여 지원서 내용물 외의 질문을 던지신 것으로 보입니다.
 

## 조언
 지금까지의 이야기는 다만 제호 경험에 비추어 작성한 이야기였을 뿐입니다. 여러분이라면 내절로 본인의 이야기를 잘 풀어나가실 것이라고 생각합니다. 본인의 장래 성장가능성을 보고 장학재단에서 장학금을 지급하는 것이기 그러니까 본인의 이야기를 당당하고 주체적으로 풀어나가시기를 추천드립니다. 제가 작성한 내용은 단지, 아 이렇게 작성하는 사람도 있구나, 정도로 여겨주시면 좋을 것 같습니다.
또한, 장학금은 회 수상과 다르다는 점도 알아두시면 좋을 것 같습니다. 주인 잘한 사람이 상을 받는 대회와 다르게 장학금은 가정형편, 소득분위, 절실함, 전공 균형 등을 고려할 수밖에 없습니다. 하여 설혹 장학금을 수령하지 못한다고 하여도 본인이 부족한 것이 아닐 수 있다는 뜻입니다. 급기야 과하게 기죽을 필요는 없다고 말씀드리고 싶습니다.
 

## 마무리
 금대 글도 유익하셨기를 바랍니다. 많은 심리 부탁드립니다. 읽어주셔서 감사합니다.
[장학금 해당 다른 글]
 9, 10분위 대학생을 위한 실리 분위 경계 없는 장학금 추천 (후기)
소득 분위 안보는 장학금 1. 일주학술문화재단 2022 해우 학원 장학 선발 공지
 

### '일상 > 대학생 라이프' 카테고리의 다른 글
